AYUDANTÍAS DE REACT - GRUPO 3
---
Ejercicios para prácticar el desarrollo de aplicaciones web con Reactjs.

### Obtener
```
git clone --recursive https://gitlab.com/desafiolatam-react-g3/teaching-assistantships-reactjs.git
```

### Listado de ejercicios
- [Comparar tarjetas](https://gitlab.com/desafiolatam-react-g3/1.reactjs-compare-cards)
- [Canasta virtual](https://gitlab.com/desafiolatam-react-g3/2.reactjs-virtual-basket)
- [Pokedex](https://gitlab.com/desafiolatam-react-g3/3.reactjs-pokedex)
- [Pronóstico del tiempo](https://gitlab.com/desafiolatam-react-g3/4.reactjs-weather-forecast)
- [Buscador de Spotify](https://gitlab.com/desafiolatam-react-g3/5.reactjs-spotify-search)

## Recursos
- [Documentación oficial de Reactjs](https://reactjs.org/docs)
- [Documentación oficial de Reduxjs](https://redux.js.org/)
- [Repositorio oficial de Create React App](https://github.com/facebook/create-react-app)

***
© [DesafioLatam](https://desafiolatam.com) - Todos los derechos reservados
